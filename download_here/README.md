# What is what

In this folder you will find the following hosts files, which is different
combinations of the source files from the `test_results/*.active.txt` folder.

These records have been imported from [Porn-Records][PR] and tested with 
[PyFunceble](https://mypdns.org/pyfunceble/PyFunceble)
To minimize the numbers of FP's ([False Positives](https://mypdns.org/mypdns/support/-/wikis/False-Positive))

```shell
download_here/
├── 0.0.0.0
│   └── hosts
├── 127.0.0.1
│   └── hosts
├── mobile
│   └── hosts
├── safesearch
│   ├── 0.0.0.0
│   │   └── hosts
│   ├── 127.0.0.1
│   │   └── hosts
│   ├── mobile
│   │   └── hosts
│   └── strict
│       ├── 0.0.0.0
│       │   └── hosts
│       └── 127.0.0.1
│           └── hosts
└── strict
    ├── 0.0.0.0
    │   └── hosts
    └── 127.0.0.1
        └── hosts
```

| Location                          | Content combination                                      | IP-address |
| :-------------------------------- | :------------------------------------------------------- | ---------: |
| 0.0.0.0/hosts                     | `hosts.txt`                                              |    0.0.0.0 |
| 127.0.0.1/hosts                   | `hosts.txt`                                              |  127.0.0.1 |
| mobile/hosts                      | `hosts.txt` + `mobile.txt`                               |    0.0.0.0 |
| safesearch/0.0.0.0/hosts          | `hosts.txt` + `safe search records`                      |    0.0.0.0 |
| safesearch/127.0.0.1/hosts        | `hosts.txt` + `safe search records`                      |  127.0.0.1 |
| safesearch/mobile/hosts           | `hosts.txt` + `safe search records`                      |    0.0.0.0 |
| safesearch/strict/0.0.0.0/hosts   | `hosts.txt` + `strict_adult.txt` + `safe search records` |    0.0.0.0 |
| safesearch/strict/127.0.0.1/hosts | `hosts.txt` + `strict_adult.txt` + `safe search records` |  127.0.0.1 |
| strict/0.0.0.0/hosts              | `hosts.txt` + `strict_adult.txt`                         |    0.0.0.0 |
| strict/127.0.0.1/hosts            | `hosts.txt` + `strict_adult.txt`                         |  127.0.0.1 |

[PR]: https://mypdns.org/my-privacy-dns/porn-records/-/tree/master

-----

## Other files

- [Porn-Records HOSTS file: **Normal**](https://mypdns.org/clefspeare13/pornhosts/-/raw/master/download_here/0.0.0.0/porn.txt)
  - Combined data from [Porn Records](https://mypdns.org/my-privacy-dns/porn-records)' `adult.mypdns.cloud` lists. [^1]
- [Porn-Records HOSTS file: **Strict**](https://mypdns.org/clefspeare13/pornhosts/-/raw/master/download_here/0.0.0.0/porn_strict.txt)
  - Combined data from [Porn Records](https://mypdns.org/my-privacy-dns/porn-records)' `strict.adult.mypdns.cloud` lists. [^2]
- [Porn-Records HOSTS file: **Fast Ring**](https://mypdns.org/clefspeare13/pornhosts/-/raw/master/download_here/0.0.0.0/porn_fastring.txt)
  - `Data from Normal` + _existing open issues_. If you want to block all currently suspected porn domains (with some risk of false positive)
- [Porn-Records HOSTS file: **Fast Ring + Strict**](https://mypdns.org/clefspeare13/pornhosts/-/raw/master/download_here/0.0.0.0/porn_fastring_strict.txt)
  - `Data from Strict` + _existing open issues_. If you want to block all currently suspected porn domains (with some risk of false positive)


[^1]: Combination of the following sources [Porn Records]: 
`adult.mypdns.cloud/domains.list`
`adult.mypdns.cloud/hosts.list`
`adult.mypdns.cloud/mobile.list`
`adult.mypdns.cloud/snuff.list`
`adult.mypdns.cloud/wildcard.list`


[^2]: Combination of the following sources [Porn Records]: 
`adult.mypdns.cloud/domains.list`
`adult.mypdns.cloud/hosts.list`
`adult.mypdns.cloud/mobile.list`
`adult.mypdns.cloud/snuff.list`
`adult.mypdns.cloud/wildcard.list`
`strict.adult.mypdns.cloud/strict.domains.list`
`strict.adult.mypdns.cloud/strict.hosts.list`
`strict.adult.mypdns.cloud/strict.wildcard.list`

[Porn Records]: https://mypdns.org/my-privacy-dns/porn-records
