# Submit - Contribute

All commits of new NSFW adult records should be done to [Matrix][MTX]

You can use the following quick links

| Category              | Commit issue                                                                             |
| :-------------------- | :--------------------------------------------------------------------------------------- |
| Adult contents        | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=NSFW_Porn        |
| Adult CDN             | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=Adult_CDN        |
| Strict Adult contents | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=NSFW_Strict      |
| Strict Adult CDN      | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=Strict_Adult_CDN |
| Snuff & gore          | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=NSFW_Snuff       |
| Common support        | https://mypdns.org/mypdns/support/-/issues/new                                           |
| Common wiki           | https://mypdns.org/MypDNS/support/-/wikis/                                               |


[MTX]: https://mypdns.org/my-privacy-dns/matrix
