[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/X8X37FUGU)

# pornhosts -- a consolidated anti porn hosts file

This is an endeavour to find relevant pornographic domains and compile
them into hosts files to allow for easy blocking of porn on your local
machine or on a network.

Since Github took down [https://github.com/clefspeare13/pornhosts][GCP]
one of the best maintained Anti porn hosts project, and forced us to
moved the project to [My Privacy DNS][CP] GIT, where it will remain
actively maintained as you are used to.

![take down notice by GitHub](https://user-images.githubusercontent.com/44526987/137626720-e2fdc828-4566-4f32-851c-acc679429f66.png)

In the future, you will be reporting any new issues to the pornhost's
"master" project at [porn-records][PR].

Do to the take down by Github the earlier announced time for merging the
sourced records from the {- clefspeare13/pornhosts -} into
[Pron-Records][PR] have arrived and have happened when you read
this README. It means all records now are 100% controlled by
[Pron-Records][PR] without exceptions.

This means all future builds are done from the earlier `submit_here`
folder + the [matrix][build-matrix] of files used to build a hosts,
Pi-hole and AdGuard blacklist

You can use the following quick links

| Category              | Commit issue                                                                             |
|:----------------------|:-----------------------------------------------------------------------------------------|
| Adult contents        | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=NSFW_Porn        |
| Adult CDN             | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=Adult_CDN        |
| Strict Adult contents | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=NSFW_Strict      |
| Strict Adult CDN      | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=Strict_Adult_CDN |
| Snuff & gore          | https://mypdns.org/my-privacy-dns/matrix/-/issues/new?issuable_template=NSFW_Snuff       |
| Common support        | https://mypdns.org/mypdns/support/-/issues/new                                           |
| Common wiki           | https://mypdns.org/MypDNS/support/-/wikis/                                               |

## Hosts file location
In order to add this to your computer, copy any of the
[hosts](https://mypdns.org/clefspeare13/pornhosts/-/raw/master/download_here/)
files, and add it to your ```hosts``` file which can be found in the
following locations:

macOS X, iOS, Android, Linux, Unix: `/etc/hosts`.

Windows: `%SystemRoot%\system32\drivers\etc\hosts`.

There are currently teen different ```hosts``` files in this repository,
which uses ```0.0.0.0``` and ```127.0.0.1``` as the stop IP. If you are
not sure which is right, the a rule of thumb is to use ```0.0.0.0``` as
it is faster and will run on essentially all machines.

However, if you know what you're doing and need a ```127.0.0.1```
version, it is available [here](https://mypdns.org/clefspeare13/pornhosts/-/raw/master/download_here/127.0.0.1/hosts)

Additionally, there is a new hosts file which will force Safe Search in
site likes `Bing` and `Google`, however it has not been tested yet. It
can be found
[here](https://mypdns.org/clefspeare13/pornhosts/-/raw/master/download_here/safesearch/0.0.0.0/hosts)

For other optional lists, please see the [README](https://mypdns.org/clefspeare13/pornhosts/-/blob/master/download_here/README.md)
in the `download_here` folder

Any helpful additions are appreciated.

If you would have an idea on how this project is constructed, please read
the [README](https://mypdns.org/my-privacy-dns/porn-records/-/blob/master/submit_here/README.md).

## Why should I contribute
You should contribute to this list because it does matter for those who
have to block this kind of content.

Let's have a look at Cloudflares <https://cloudflare-dns.com/family/>
so called adult filter running on `1.1.1.3`

![Cloudflare-dns adult filtering](https://archive.mypdns.org/file/data/lethgvoookfqugdffqjk/PHID-FILE-fsnlpmklbe5rnalbjlip/preview-image.png)

From the test file
`https://mypdns.org/clefspeare13/pornhosts/-/blob/master/0.0.0.0/hosts`
which we are going to use for our test we see the following result and
why it matters you are contributing.

## Test result

| Status   | Percentage | Numbers |
|:---------|-----------:|--------:|
| ACTIVE   |        96% |    8615 |
| INACTIVE |         3% |     356 |
| INVALID  |         0% |       0 |

## Conclusion
We can hereby conclude this project have knowledge to 8615 domains, which
CloudFlare-dns do not know about

<!-- links -->
[GCP]: https://mypdns.org/clefspeare13/pornhosts
[CP]: https://mypdns.org/clefspeare13/pornhosts
[PR]: https://mypdns.org/my-privacy-dns/porn-records
[build-matrix]: https://mypdns.org/my-privacy-dns/porn-records#file-structure-explained
