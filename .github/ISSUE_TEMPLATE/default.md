---
name: New submission
about: I would like to contribute with the following domain to the hosts file
title: ''
labels: NSFW Adult Material
assignees: Spirillen
---

I believe this domain is an Adult(-related) domain --> that have to be blocked as..

## Domains
Only add the needed one See also #12

<!-- usage of www or not
Please check if you submission is using the the www or not and put that into the section of

You can tell us you have checked this by adding either a + or a -
in front of the www

+ www  The domain uses **both** the `www` and the //non// `www` names.
- www  The domain uses **only** the //non// `www` name.
www.domain  The domain uses **only** the `www.` name.
www.? Leaving the question mark tells us you haven't tested this
-- >

```python

www.
```

## Content sources
<!-- Any external domains used for hosting centents on this sire (aka CDN) -->

(Please see #35)

```python


```

## External sources (reference)
## External Info
-
-
-

## Screenshots

<details><Summary>Screenshot required</summary>


</details>


### All Submissions:
- [ ] Have you checked to ensure there aren't other open [Merge Requests (MR)](../merge_requests) or [Issues](../issues) for the same update/change?
- [ ] Added a Screenshot for prove of Adult contents

### Todo
- [ ] Added to [Source file](submit_here/hosts.txt)?
- [ ] Added to the RPZ zone [adult.mypdns.cloud](https://www.mypdns.org/w/rpzlist/#adult-mypdns-cloud) (@spirillen)
