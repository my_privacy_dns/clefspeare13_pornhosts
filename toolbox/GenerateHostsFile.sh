#!/usr/bin/env bash

# Fail if exit != 0
set -e

git pull --rebase

# Run script in verbose
# set -x

# printf "\n\tRunning %s\n\n" "${0}"

# *********************************************************************
# Define root dir for git, for working with relative dir to this repo #
# *********************************************************************

GIT_DIR="$(git rev-parse --show-toplevel)"

# ******************
# Set Some Variables
# ******************

TAG=$(date '+%d. %b %Y %T %z (%Z)')
MY_GIT_TAG="Build: $(date '+%j')"

# **********************************************************************
# Set some dirs
# **********************************************************************

# BASEURL="https://mypdns.org/clefspeare13/pornhosts/-/raw/master/active_domains/output"
BASE_PATH="${GIT_DIR/active_domains/output}"

# BASEURL="${GIT_DIR}/active_domains/output"
OUTDIR="${GIT_DIR}/download_here" # no trailing / as it would make a double //

# *******************************************
# Import sources for building new lists
# *******************************************

# The devine ones...
ECD="${BASE_PATH}/explicit_content.domains.list/domains/ACTIVE/list"
ECH="${BASE_PATH}/explicit_content.hosts.list/domains/ACTIVE/list"
ECM="${BASE_PATH}/explicit_content.mobile.list/domains/ACTIVE/list"
ECS="${BASE_PATH}/explicit_content.snuff.list/domains/ACTIVE/list"
ECW="${BASE_PATH}/explicit_content.wildcard.list/domains/ACTIVE/list"

# The Imported...
IAS="${BASE_PATH}/imported.adult.ShadowWhisperer/domains/ACTIVE/list"
IDM="${BASE_PATH}/imported.domains.matrix/domains/ACTIVE/list"
IHC="${BASE_PATH}/imported.hosts.clefspeare13/domains/ACTIVE/list"

# The very strict once...
SFD="${BASE_PATH}/strict_filters.domains.list/domains/ACTIVE/list"
SFH="${BASE_PATH}/strict_filters.hosts.list/domains/ACTIVE/list"
SFW="${BASE_PATH}/strict_filters.wildcard.list/domains/ACTIVE/list"

# Temporary files for building the lists
# Create a temporary directory and store its name in a variable ...

STRAIGHT_REC="$(mktemp)"
MOBILE_REC="$(mktemp)"
SNUFF_REC="$(mktemp)"
STRICT_REC="$(mktemp)"
WILDCARD_REC="$(mktemp)"
WILDCARD_STRICT_REC="$(mktemp)"

# **********************************************************************
# Ordinary OUTPUT without safe search records
# **********************************************************************
HOSTS="${OUTDIR}/0.0.0.0/hosts"
HOSTS127="${OUTDIR}/127.0.0.1/hosts"
MOBILE="${OUTDIR}/mobile/hosts"
AD_BLOCKERS="${OUTDIR}/ad_blockers/ad_blockers.filter"
STRICT="${OUTDIR}/strict/0.0.0.0/hosts"
STRICT127="${OUTDIR}/strict/127.0.0.1/hosts"
AD_BLOCKERS_STRICT="${OUTDIR}/strict/ad_blockers/ad_blockers.strict"

# **********************************************************************
# Safe Search enabled OUTPUT
#
# no trailing / as it would make a double //
# **********************************************************************
SSOUTDIR="${OUTDIR}/safesearch"
SSHOSTS="${SSOUTDIR}/0.0.0.0/hosts"
SSHOSTS127="${SSOUTDIR}/127.0.0.1/hosts"
SSMOBILE="${SSOUTDIR}/mobile/hosts"
SSSTRICT="${SSOUTDIR}/strict/0.0.0.0/hosts"
SSSTRICT127="${SSOUTDIR}/strict/127.0.0.1/hosts"

# ****************
# End of variables
# ****************

function delOutPutDir() {
    find "${OUTDIR}" ! \( -iname "*.md" -o -iname "*.txt" \) -type f -delete
}

# function cleanTemp () {
#     find /tmp/ -mindepth 1 -maxdepth 1 -iname "tmp.*" -delete
# }

# **************
# Script at work
# **************
# First let us clean up old data in output folders
delOutPutDir

# **********************************************************************
# Next ensure all output folders is there
# **********************************************************************
mkdir -p \
    "${OUTDIR}/0.0.0.0" \
    "${OUTDIR}/127.0.0.1" \
    "${OUTDIR}/mobile" \
    "${OUTDIR}/strict/0.0.0.0/" \
    "${OUTDIR}/strict/127.0.0.1/" \
    "${OUTDIR}/ad_blockers/" \
    "${OUTDIR}/strict/ad_blockers/" \
    "${SSOUTDIR}/0.0.0.0" \
    "${SSOUTDIR}/127.0.0.1" \
    "${SSOUTDIR}/mobile" \
    "${SSOUTDIR}/strict/0.0.0.0/" \
    "${SSOUTDIR}/strict/127.0.0.1/"

# **********************************************************************
# Import and sort lists to generate the hosts files
# **********************************************************************

{
    wget -qO - "$ECH"
    wget -qO - "$IHC"
    wget -qO - "$IAS"
    wget -qO - "$IDM"
    wget -qO - "$SRC_IMP_PHIES"
    wget -qO - "$ECD"
    wget -qO - "$ECW"
} | awk '/^(#|$)/{ next }; { if ( $0 ~ /[a-z]/ ) print tolower($0) | "sort -t. -u" }' >> \
    "$STRAIGHT_REC"

{
    wget -qO - "$SRC_IMP_MOBILE"
    wget -qO - "$ECM"
} | awk '/^(#|$)/{ next }; { if ( $0 ~ /[a-z]/ ) print tolower($0) | "sort -t. -u" }' >> \
    "$MOBILE_REC"

{
    wget -qO - "$ECS"
} | awk '/^(#|$)/{ next }; { if ( $0 ~ /[a-z]/ ) print tolower($0) | "sort -t. -u" }' >> \
    "$SNUFF_REC"

{
    wget -qO - "$SRC_IMP_STRICT"
    wget -qO - "$SFD"
    wget -qO - "$SFH"
    wget -qO - "$SFW"
} | awk '/^(#|$)/{ next }; { if ( $0 ~ /[a-z]/ ) print tolower($0) | "sort -t. -u" }' >> \
    "$STRICT_REC"

{
    wget -qO - "$IHC"
    wget -qO - "$ECD"
    wget -qO - "$ECW"
    wget -qO - "$SRC_IMP_PHIES"
} | awk '/^(#|$)/{ next }; {gsub(/^[www.]*/,"")}; { if ( $0 ~ /[a-z]/ ) 
    print tolower($0) | "sort -t. -u" }' >>"$WILDCARD_REC"

{
    wget -qO - "$SRC_IMP_STRICT"
    wget -qO - "$SFD"
    wget -qO - "$SFW"
} | awk '/^(#|$)/{ next }; {gsub(/^[www.]*/,"")}; { if ( $0 ~ /[a-z]/ )
    print tolower($0) | "sort -t. -u" }' >>"$WILDCARD_STRICT_REC"

# **********************************************************************
# Set templates path
# **********************************************************************
TEMPLATE_PATH="${GIT_DIR}/toolbox/templates"

HOST_TEMPLATE="${TEMPLATE_PATH}/hosts.template"
MOBILE_TEMPLATE="${TEMPLATE_PATH}/mobile.template"
AD_BLOCKERS_TEMPLATE="${TEMPLATE_PATH}/ad_blockers.template"

# **********************************************************************
# Safe Search is in sub-path
# **********************************************************************
SSTEMPLATE_PATH="${TEMPLATE_PATH}/safesearch"
SSHOST_TEMPLATE="${SSTEMPLATE_PATH}/safesearch.template" # same for mobile

# **********************************************************************
echo "Update our safe search templates"
# **********************************************************************
# Append this to the bottom of the Safesearch generated files

wget -qO "${SSHOST_TEMPLATE}" \
    'https://mypdns.org/my-privacy-dns/matrix/-/raw/master/safesearch/safesearch.hosts'

touch "${HOSTS}" "${HOSTS127}" "${STRICT}" "${STRICT127}" "${SSHOSTS}" \
    "${SSHOSTS127}" "${SSSTRICT}" "${SSSTRICT127}" "${MOBILE}" "${SSMOBILE}" \
    "${AD_BLOCKERS}" "${AD_BLOCKERS_STRICT}"

# ***********************************
# Print the header in all hosts files
# ***********************************

echo "# Blacklist generated: ${TAG}" "${MY_GIT_TAG}" | tee -ai \
    "${HOSTS}" "${HOSTS127}" "${MOBILE}" \
    "${AD_BLOCKERS_STRICT}" "${STRICT}" "${STRICT127}" "${SSHOSTS}" \
    "${SSHOSTS127}" "${SSMOBILE}" "${SSSTRICT}" "${SSSTRICT127}" >>"${AD_BLOCKERS}"

# *************************************
# Import templates into the hosts files
# *************************************

< "${HOST_TEMPLATE}" tee -ai "${HOSTS127}" "${STRICT}" \
    "${STRICT127}" "${SSHOSTS}" "${SSHOSTS127}" "${SSSTRICT}" \
    "${SSSTRICT127}" >>"${HOSTS}"

< "${MOBILE_TEMPLATE}" tee -ai "${SSMOBILE}" >>"${MOBILE}"

< "${AD_BLOCKERS_TEMPLATE}" tee -ai \
    "${AD_BLOCKERS_STRICT}" >>"${AD_BLOCKERS}"

# **********************************************************************
echo ""
echo 'Appending the MOBILE template to the mobile lists only'
#
# We should keep these in the top of the hosts file, to minimize the I/O
# For traversing through the file over and over and over....
# See https://www.mypdns.org/w/dnshosts/ For more info.
# **********************************************************************
printf "\n# Mobile domains\n" >>"${MOBILE}"
printf "\n# Mobile domains\n" >>"${SSMOBILE}"

awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${MOBILE_REC}" >>"${MOBILE}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${MOBILE_REC}" >>"${SSMOBILE}"

# **********************************************************************
echo ""
echo "Generating PORN hosts"
# **********************************************************************

echo -e "\n# Porn Records" | tee -ai "${HOSTS}" \
    "${HOSTS127}" "${STRICT}" "${STRICT127}" "${AD_BLOCKERS_STRICT}" "${SSHOSTS}" \
    "${SSHOSTS127}" "${SSSTRICT}" "${SSSTRICT127}" "${MOBILE}" "${SSMOBILE}" >>"${AD_BLOCKERS}"

# Standard 0.0.0.0
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${HOSTS}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${MOBILE}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${STRICT}"

# SafeSearch
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${SSHOSTS}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${SSMOBILE}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${SSSTRICT}"

# Windows 127.0.0.1
awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${HOSTS127}"
awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${STRICT127}"
awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${SSHOSTS127}"
awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${STRAIGHT_REC}" >>"${SSSTRICT127}"

# Ad Blockers with wildcard
awk '{ printf("||%s^\n"), $1 }' "${WILDCARD_REC}" >>"${AD_BLOCKERS}"
awk '{ printf("||%s^\n"), $1 }' "${WILDCARD_REC}" >>"${AD_BLOCKERS_STRICT}"

# **********************************************************************
echo ""
echo "Adding SNUFF to hosts"
# **********************************************************************

printf "\n# snuff domains\n" | tee -ai "${HOSTS}" \
    "${HOSTS127}" "${AD_BLOCKERS_STRICT}" "${STRICT}" "${STRICT127}" \
    "${SSHOSTS}" "${SSHOSTS127}" "${SSSTRICT}" "${SSSTRICT127}" \
    "${MOBILE}" "${SSMOBILE}" >>"${AD_BLOCKERS}"

# Standard 0.0.0.0
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${HOSTS}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${MOBILE}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${STRICT}"

# SafeSearch
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${SSHOSTS}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${SSMOBILE}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${SSSTRICT}"

# Windows 127.0.0.1
awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${HOSTS127}"
awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${STRICT127}"
awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${SSHOSTS127}"
awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${SNUFF_REC}" >>"${SSSTRICT127}"

# Ad Blockers with wildcard
awk '{ printf("||%s^\n"), $1 }' "${SNUFF_REC}" >>"${AD_BLOCKERS}"
awk '{ printf("||%s^\n"), $1 }' "${SNUFF_REC}" >>"${AD_BLOCKERS_STRICT}"

# **********************************************************************
echo ""
echo "Generate Grey Area"
# **********************************************************************
printf "\n# strict domains\n" >>"${STRICT}"
printf "\n# strict domains\n" >>"${STRICT127}"
printf "\n# strict domains\n" >>"${SSSTRICT}"
printf "\n# strict domains\n" >>"${SSHOSTS127}"
printf "\n# strict domains\n" >>"${AD_BLOCKERS_STRICT}"

awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${STRICT_REC}" >>"${STRICT}"
awk '{ printf("0.0.0.0 %s\n",tolower($1)) }' "${STRICT_REC}" >>"${SSSTRICT}"

awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${STRICT_REC}" >>"${STRICT127}"
awk '{ printf("127.0.0.1 %s\n",tolower($1)) }' "${STRICT_REC}" >>"${SSHOSTS127}"

# Ad Blockers with wildcard
awk '{ printf("||%s^\n"), $1 }' "${WILDCARD_STRICT_REC}" >>"${AD_BLOCKERS_STRICT}"

# *******************************************
echo ""
echo "Appending the SafeSeach template"
# *******************************************

printf "\n\n" >>"${SSHOSTS}"
printf "\n\n" >>"${SSHOSTS127}"
printf "\n\n" >>"${SSMOBILE}"
printf "\n\n" >>"${SSSTRICT}"
printf "\n\n" >>"${SSSTRICT127}"

< "$SSHOST_TEMPLATE" tee -a "${SSHOSTS127}" \
    "${SSMOBILE}" "${SSSTRICT}" "${SSSTRICT127}" >>"${SSHOSTS}"

# *****
# Clean tmp files and dirs
# *******

# cleanTemp

cd "${GIT_DIR}"

git add .
git commit -a -m "Publish Porn blacklists. Updated: ${TAG} ${MY_GIT_TAG}"
git pull --rebase
git push

exit ${?}

# Copyright: https://mypdns.org/
# Content: https://mypdns.org/Spirillen/
# Source: https://github.com/Import-External-Sources/pornhosts
# License: https://mypdns.org/mypdns/support/-/wikis/License
# License Comment: GNU AGPLv3, MODIFIED FOR NON COMMERCIAL USE
#
# License in short:
# You are free to copy and distribute this file for non-commercial uses,
# as long the original URL and attribution is included.
#
# Please forward any additions, corrections or comments by logging an
# issue at https://mypdns.org/my-privacy-dns/porn-records
