#!/usr/bin/env bash

# version 1.0.1b1

# availability test before publishing new results

# Run script in verbose
# set -x

# Fail if exit != 0
set -e

# *********************************************************************
# Define root dir for git, for working with relative dir to this repo #
# *********************************************************************

echo "Setting git root dir"

GIT_DIR="$(git rev-parse --show-toplevel)"

cd "$GIT_DIR" || exit 1

echo "Rebasing before test"

git fetch && git pull --rebase

echo -e "\n\tRunning ${0}\n\n"

PYTHON_VERION="3.10"

echo "Setting python version: $PYTHON_VERION"

echo "setting system variables"

POL_LIBRARY="/tmp/pyfunceble"
AD="${GIT_DIR}/active_domains"

BASE_URL="https://mypdns.org/my-privacy-dns/matrix/-/raw/master/source/porn_filters"
ADULT_URI="$BASE_URL/explicit_content"
IMPORT_LIBRARY_UIR="$BASE_URL/imported"
STRICT_URI="$BASE_URL/strict_filters"

DOMAINS="${ADULT_URI}/domains.list"
HOSTS="${ADULT_URI}/hosts.list"
MOBILE="${ADULT_URI}/mobile.list"
RPZIP="${ADULT_URI}/rpz-ip"
SNUFF="${ADULT_URI}/snuff.list"
WILDCARD="${ADULT_URI}/wildcard.list"
RPZNSDNAME="${ADULT_URI}/wildcard.rpz-nsdname"

IMP_SW="${IMPORT_LIBRARY_UIR}/adult.ShadowWhisperer"
IMP_DOMAINS="${IMPORT_LIBRARY_UIR}/domains.matrix"
IMP_HOSTS="${IMPORT_LIBRARY_UIR}/hosts.clefspeare13"

STRICT_DOMAIN="${STRICT_URI}/domains.list"
STRICT_HOSTS="${STRICT_URI}/hosts.list"
STRICT_RPZIP="${STRICT_URI}/strict.rpz-ip"
STRICT_WILDCARD="${STRICT_URI}/wildcard.list"
STRICT_RPZNSDNAME="${STRICT_URI}/wildcard.rpz-nsdname"

# Temp files
ECD="$AD/explicit_content.domains.list"
ECH="$AD/explicit_content.hosts.list"
ECM="$AD/explicit_content.mobile.list"
ECR="$AD/explicit_content.rpz-ip"
ECS="$AD/explicit_content.snuff.list"
ECW="$AD/explicit_content.wildcard.list"
ECRDNS="$AD/explicit_content.wildcard.rpz-nsdname"

IAS="$AD/imported.adult.ShadowWhisperer"
IDM="$AD/imported.domains.matrix"
IHC="$AD/imported.hosts.clefspeare13"

SFD="$AD/strict_filters.domains.list"
SFH="$AD/strict_filters.hosts.list"
SFS="$AD/strict_filters.strict.rpz-ip"
SFW="$AD/strict_filters.wildcard.list"
SFRDNS="$AD/strict_filters.wildcard.rpz-nsdname"

# For testing
# testFile="https://raw.githubusercontent.com/PyFunceble/ci_test/master/test.list"

echo "Setup PyFunceble environment"

# echo "Make sure Conda is installed"

# if [ ! -d "${GIT_DIR}/miniconda" ]; then
#     echo "installing miniconda"
#     echo ""
#     exec "$GIT_DIR/toolbox/conda_install.sh"
# fi

# if [ -f "${GIT_DIR}/miniconda/etc/profile.d/conda.sh" ]; then
#     source "${GIT_DIR}/miniconda/etc/profile.d/conda.sh"
# else
#     exec "$GIT_DIR/toolbox/conda_install.sh"
#     echo "installing miniconda"
#     echo ""
# fi

echo "Make sure we have the $AD folder"
if [ ! -d "$AD" ]; then
    mkdir "$AD"
fi

echo "Ensure local PyFunceble configurations are present"

if [ ! -f "$HOME/.config/PyFunceble/.pyfunceble-env" ]; then
    echo Missing config file
    exit 31
fi

if [ ! -f "$HOME/.config/PyFunceble/.PyFunceble.overwrite.yaml" ]; then
    echo Missing config file
    exit 32
fi

if [ ! -f "$HOME/.config/PyFunceble/.PyFunceble.yaml" ]; then
    echo Missing config file
    exit 33
fi

# Do to double naming of source files, we will need to download them to a local
# temp location first

# Downlaod source files
echo "Importing sources"

WGET_ARGS="--tries=5 -c --no-check-certificate -q"

wget "$WGET_ARGS" -O "${ECD}" "${DOMAINS}"
wget "$WGET_ARGS" -O "${ECH}" "${HOSTS}"
wget "$WGET_ARGS" -O "${ECM}" "${MOBILE}"
wget "$WGET_ARGS" -O "${ECR}" "${RPZIP}"
wget "$WGET_ARGS" -O "${ECS}" "${SNUFF}"
wget "$WGET_ARGS" -O "${ECW}" "${WILDCARD}"
wget "$WGET_ARGS" -O "${ECRDNS}" "${RPZNSDNAME}"

wget "$WGET_ARGS" -O "${IAS}" "${IMP_SW}"
wget "$WGET_ARGS" -O "${IDM}" "${IMP_DOMAINS}"
wget "$WGET_ARGS" -O "${IHC}" "${IMP_HOSTS}"

wget "$WGET_ARGS" -O "${SFD}" "${STRICT_DOMAIN}"
wget "$WGET_ARGS" -O "${SFH}" "${STRICT_HOSTS}"
wget "$WGET_ARGS" -O "${SFS}" "${STRICT_RPZIP}"
wget "$WGET_ARGS" -O "${SFW}" "${STRICT_WILDCARD}"
wget "$WGET_ARGS" -O "${SFRDNS}" "${STRICT_RPZNSDNAME}"

echo "Installing or updating PyFunceble"

# if [ -z $"{which pyfunceble}" ]; then
#     $PYTHON_VERION -m pip install -U -I --pre pyfunceble-dev >=4.1.3a2
# fi

# hash conda

# echo "Update Conda"
# conda update conda -c conda-canary
# conda update -yq conda
# conda config --set channel_priority false

# echo "Remove old conda ENV"
# conda env remove -n pyfunceble

# if [ -n "$(conda info --envs | cut -d ' ' -f 1 | grep -i 'pyfunceble')" ]; then
#     echo "Update conda ENV"
#     conda create -q -n pyfunceble \
#         -f "${GIT_DIR}/toolbox/.environment.yaml" \
#         --prefix "${GIT_DIR}/miniconda" --all
# else
#     echo "Install fresh conda ENV"
#     conda env update -n pyfunceble \
#         -f "${GIT_DIR}/toolbox/.environment.yaml" --prune
# fi

# echo "Activating conda ENV"
# conda activate pyfunceble

python3.10 -m pip install -U -I --pre pyfunceble-dev

echo "Setting global PyFunceble ENV"
export PYFUNCEBLE_AUTO_CONFIGURATION=YES
export PYFUNCEBLE_DEBUG=True
export PYFUNCEBLE_DEBUG_LVL=critical

hash pyfunceble

printf "\n\tYou are running with Pyfunceble\n\n"

function pyfunceble_basic() {

    if [ ! -d "${POL_LIBRARY}" ]; then
        echo "PyFunceble Output Location do not exist, Creating"
        echo ""
        mkdir -p "${POL_LIBRARY}"
        mount "${POL_LIBRARY}"
    else
        echo "PyFunceble Output Location do exist, Deleting"
        echo ""
        rm -fr "${POL_LIBRARY:?}/*"
        umount "${POL_LIBRARY}"
        echo "PyFunceble Output Location do exist, Recreate"
        echo ""
        mkdir -p "${POL_LIBRARY}"
        mount "${POL_LIBRARY}"
    fi

    echo "Switching to PyFunceble Output Location Directory"
    echo ""
    cd "${POL_LIBRARY}" || exit 2

    echo "Setting args for pyfunceble config- and output-dir"
    echo ""
    export PYFUNCEBLE_CONFIG_DIR="${HOME}/.config/PyFunceble/"
    export PYFUNCEBLE_OUTPUT_LOCATION="${POL_LIBRARY}"

    echo "Copying previously test results to $POL_LIBRARY"
    echo ""
    if [ -d "$AD" ]; then
        rsync -rvP --exclude rpz.mypdns.cloud/ \
            --delete-before "$AD/" "${POL_LIBRARY}/"
    fi

    echo ""
    echo "pyfunceble --version"
    echo ""
    pyfunceble --version

    echo "Executing PyFunceble"
    # pyfunceble \
    #     --database-type csv \
    #     -f "${DOMAINS}" \
    #     "${HOSTS}" \
    #     "${MOBILE}" \
    #     "${RPZIP}" \
    #     "${SNUFF}" \
    #     "${WILDCARD}" \
    #     "${RPZNSDNAME}" \
    #     "${IMP_SW}" \
    #     "${IMP_DOMAINS}" \
    #     "${IMP_HOSTS}" \
    #     "${STRICT_DOMAIN}" \
    #     "${STRICT_HOSTS}" \
    #     "${STRICT_RPZIP}" \
    #     "${STRICT_WILDCARD}" \
    #     "${STRICT_RPZNSDNAME}"

    pyfunceble --collection-lookup \
        --database-type csv \
        -f "$ECD" "$ECH" "$ECM" "$ECR" "$ECS" "$ECW" "$ECRDNS" \
        "$IAS" "$IDM" "$IHC" \
        "$SFD" "$SFH" "$SFS" "$SFW" "$SFRDNS"

    # Temponary to continue lastest run
    # pyfunceble \
    #     --database-type csv \
    #     -f "$IAS" "$SFD" "$SFH" "$SFS" "$SFW" "$SFRDNS"

    echo "Changing Directory to GIT Dir"
    cd "$GIT_DIR"

    echo "Storing test result in REPO"
    rsync -avPq "${POL_LIBRARY}/" "$AD/"
}

pyfunceble_basic

# echo "Deactivating Conda"
# conda deactivate
# echo "Removing Pyfunceble env in conda"
# conda env remove -n pyfunceble

echo "umount ${POL_LIBRARY}"
umount "${POL_LIBRARY}/"
rm -fr "${POL_LIBRARY:?}/"

echo "commit test results"
git add .
git commit -am "Latest availability test with @pyfunceble" && git push

exit ${?}

# Copyright: https://mypdns.org/
# Content: https://mypdns.org/spirillen/
# Source: https://mypdns.org/my-privacy-dns/porn-records
# License: https://mypdns.org/my-privacy-dns/porn-records/-/blob/master/LICENSE
# License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
#
# You should have received a copy of the license along with this
# work.
#
# Please forward any additions, corrections or comments by logging an
# issue at https://mypdns.org/my-privacy-dns/porn-records/-/issues
