# Toolbox
The place the repo maintainers will find the tools to keep this project
up and running.


## domain-test.sh
This script is used to test all records from the `submit_here/file`
to determine which is active records and which are dead (sub.)domain.tld

This is a powerful tool to maintain the project as a valid and trustworthy
source of records.


## GenerateHostsFile.sh
The `GenerateHostsFile.sh` is used for generating the the `hosts` file
in various formats.

1. 0.0.0.0/hosts
2. 127.0.0.1/hosts
3. Mobile_0_0_0_0/hosts
4. SafeSearch/hosts
5. strict/hosts
